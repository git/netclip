
/*

  netclip.h -- headerfile for netclip
  Copyright (C) 2017  Charlie Sale

  This file is part of netclip

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

#ifndef NETCLIP_H
#define NETCLIP_H

#include <sys/queue.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdarg.h>
#include <syslog.h>

/* Request macros */
#define NCR_KILL  0 // kill server
#define NCR_CREAT 1 // copy
#define NCR_READ  2 // get
#define NCR_GET   3 // pop

// color stuff
#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_RESET   "\x1b[0m"

#define print_error(s, f) fprintf(stderr, ANSI_COLOR_RED s ANSI_COLOR_RESET, f);

#define NC_DEFAULT_SIZE   32
#define NC_DEFAULT_PORT   8899

/* Declaration of types */

/* clip_t -- represents a text clip */
typedef struct clip
{
  char *buffer; // buffer of data
  char *sender; // host from where it was gotten
  int   size;   // size of data
  //  int   only_s; // return clipping only to the original sender
} clip_t;

typedef struct clip_stack
{
  clip_t **clip_array; // array of clipings
  int      top_idx;    // index of top item
  int      max_size;   // total allowed size of stack
} clip_stack_t;

typedef struct _nc_session
{
  int     conn;       // connection file descriptor
  clip_t *clip;       // current working clip
  int     stack_stat; // status of stack, 0 is ok, -1 empty, 1 full
} nc_session_t;

/* Declaration of functions */

/* Stack functions */

/* Creates a new clip:
   char* --> data to place inside clip
   clip_t* --> pointer to left clip
*/
clip_stack_t*
clip_stack_new(int);

clip_t*
clip_new(char *, char *);

/* clip_stack_free -- frees a stack */
void
clip_stack_free(clip_stack_t*);

/* clip_stack_push -- pushes a new clip onto the stack */
int
clip_stack_push(clip_stack_t*, clip_t*);

/* Get the top item without popping the stack */
clip_t*
clip_stack_get_top(clip_stack_t*);

/* clip_stack_pop -- pops a clip from the stack */
clip_t*
clip_stack_pop(clip_stack_t*);

/* Networking functions */

typedef int netclip_sock_t; // just to make a more specific type

/* Set up networking connection */

// client specific
int
netclip_cli_init(const char*, int);

// server specific
int
netclip_serv_init(int);

/* Send command */
int
netclip_net_send_cmd(int, int);

/* Send a clip */
int
netclip_net_send(int, clip_t*);

int
netclip_net_recv_cmd(int);

/* Recv a clip */
char *
netclip_net_recv(int);

void
netclip_net_close(int);

/* API functions */

int
nc_init(nc_session_t *, char *, int);

char *
nc_get(nc_session_t *);

char *
nc_pop(nc_session_t *);

int
nc_copy(nc_session_t *, char *);

int
nc_stat_stack(nc_session_t *);

int
nc_close(nc_session_t *);

#endif // NETCLIP_H
