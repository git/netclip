

/*

  clip_stack.c -- defininition of clip stack routines
  Copyright (C) 2017  Charlie Sale

  This file is part of netclip

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/


#include <netclip.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

clip_stack_t*
clip_stack_new(int size)
{
  // Create a fully initilized stack
  clip_stack_t *tmp = malloc (sizeof(clip_stack_t));  // allocate a stack obj
  if (!tmp)           return NULL;
  tmp->clip_array   = malloc (sizeof(clip_t) * size); // allocate an array
  if (!tmp->clip_array) return NULL;
  tmp->max_size     = size;
  tmp->top_idx      = 0;

  return tmp;
}

clip_t*
clip_new(char *data, char *sender)
{
  clip_t *tmp = malloc (sizeof(clip_t));
  if (!tmp)     return NULL;
  tmp->buffer = malloc (sizeof(data));
  if (!tmp->buffer) return NULL;
  memcpy(tmp->buffer, data, sizeof(data));
  if (sender != NULL)
    memcpy(tmp->sender, sender, sizeof(sender));
  else
    tmp->sender = NULL;

  tmp->size   = sizeof (tmp->buffer);

  return tmp;
}

int
clip_stack_push(clip_stack_t *stack, clip_t *clip)
{
  //  TODO: assert that there is room for the next clip
  if ((sizeof(stack->clip_array) / sizeof(clip_t)) + 1 > stack->max_size)
    return -1;

  stack->clip_array[stack->top_idx++] = clip; // place clip in top index

  return 0;
}

clip_t*
clip_stack_get_top(clip_stack_t *root)
{
  // get top increment, but don't scale it down
  return root->clip_array[root->top_idx];
}

clip_t*
clip_stack_pop(clip_stack_t *root)
{
  // get top and set it's place to null
  clip_t *clip = root->clip_array[root->top_idx];
  root->clip_array[root->top_idx--] = 0;

  return clip;
}

void
clip_stack_free(clip_stack_t *root)
{
  // free the stack's internal array and then the stack itself
  // FIXME: INVALID FREEING OF MEMORY

  int length = sizeof(root) / sizeof(clip_t);
  for (int i = 0; i < length; i++)
    {
      free(root->clip_array[i]);
    }

  //free (root);
}
