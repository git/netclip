

/*

  clip_stack.c -- defininition of clip stack routines
  Copyright (C) 2017  Charlie Sale

  This file is part of netclip

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include <netclip.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static const int GET = 0;
static const int POP = 1;

static int
__get_common(nc_session_t *, int);

static int
__do_get(nc_session_t *);

static int
__do_pop(nc_session_t *);

char *
nc_get(nc_session_t *session)
{
  int ret = __get_common(session, GET);
  if (ret < 0)
    return NULL;

  return session->clip->buffer;
}

char *
nc_pop(nc_session_t *session)
{
  int ret = __get_common(session, POP);
  if (ret < 0)
    return NULL;

  return session->clip->buffer;
}

static int
__get_common(nc_session_t *session, int cmd)
{
  int ret;
  if (cmd == GET)
    {
      ret = __do_get(session);
      if (ret < 0)
        return -1;
    }
  else if (cmd == POP)
    {
      ret = __do_pop(session);
      if (ret < 0)
        return -1;
    }
  else
    {
      return -2; // because why the fuck would this happen
    }

  return 0;
}


static int
__do_get(nc_session_t *session)
{
  int ret;

  // send command
  ret = netclip_net_send_cmd(session->conn, NCR_READ);
  if (ret < 0)
    return -1;

  char *data = netclip_net_recv(session->conn);
  if (!data)
    return -1;

  session->clip = clip_new(data, NULL);
  if (!session->clip)
    return -1;

  return 0;
}

static int
__do_pop(nc_session_t *session)
{
  int ret;

  // send command
  ret = netclip_net_send_cmd(session->conn, NCR_GET);
  if (ret < 0)
    return -1;

  char *data = netclip_net_recv(session->conn);
  if (!data)
    return -1;

  session->clip = clip_new(data, NULL);
  if (!session->clip)
    return -1;

  return 0;
}
