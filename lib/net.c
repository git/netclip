

/*

  net.c -- handles some networking protocols
  Copyright (C) 2017  Charlie Sale

  This file is part of netclip

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include <netclip.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <config.h>
#include <stdint.h>

/* begin */

#define IS_DAEMON

static int
__write_int(int, int);

static int
__read_int(int);

int
netclip_cli_init(const char *host, int port)
{
  int fd; // create a file descriptor

  struct addrinfo  hints;    // used for connecting
  struct addrinfo *servinfo; // info about server to connect to

  memset(&hints, 0, sizeof hints);
  hints.ai_family   = AF_UNSPEC;   // unspecified family
  hints.ai_socktype = SOCK_STREAM; // socket stream socket
  //  hints.ai_flags    = AI_PASSIVE;  // fill in ip for me

  // quick little conv
  char number[10];
  sprintf(number, "%d", port);

  // get connection info
  int ret = getaddrinfo(host, number, &hints, &servinfo);
  if (ret != 0)
    {
      return -1;
    }

  // create socket with address info
  fd = socket(servinfo->ai_family, servinfo->ai_socktype, servinfo->ai_protocol);
  if (fd < 0)
    {
      return -1;
    }

  ret = connect(fd, servinfo->ai_addr, servinfo->ai_addrlen);
  if (ret != 0)
    return -1;

  freeaddrinfo(servinfo);

  // socket is connected...
  return fd;
}

int
netclip_serv_init(int port)
{
  int fd;

  struct addrinfo  hints; // used for connecting
  struct addrinfo *info;  // info about address

  memset(&hints, 0, sizeof hints);

  hints.ai_family = AF_UNSPEC;
  hints.ai_socktype = SOCK_STREAM;
  hints.ai_flags = AI_PASSIVE; // autoset IP

  int status;

  char num[10];
  //itoa(port, num, 20);
  sprintf(num, "%d", port);

  status = getaddrinfo(NULL, num, &hints, &info);
  if (status != 0)
    return -1;

  fd = socket(info->ai_family, info->ai_socktype, info->ai_protocol);
  if (fd < 0)
    return -1;

  status = bind(fd, info->ai_addr, info->ai_addrlen);
  if (status != 0)
    return -1;

  freeaddrinfo(info);

  // bound
  return fd;
}

int
netclip_net_send(int fd, clip_t *clip)
{
  /*

    FIXME: Something is wrong with the sending and recieving of data

   */

  int status;

  // first, send size of data
  int buf_size = strlen(clip->buffer);

#if DEBUG
#ifdef IS_DAEMON
  syslog(LOG_INFO, "Size of outgoing buffer: %d", buf_size);
#else
  printf("Size of outgoing buffer: %d", buf_size);
#endif
#endif

  status = __write_int(fd, buf_size);
  if (status <= 0)
    return -1;

  // send actual data
  status = write(fd, clip->buffer, buf_size); // write out buffer
  if (status <= 0)
    return -1;

#if DEBUG
#ifdef IS_DAEMON
  syslog(LOG_INFO, "Amount written: %d\n", status);
#else
  printf(ANSI_COLOR_YELLOW "Buffer recieved: %d\n" ANSI_COLOR_RESET, status);
#endif
#endif

  return status;
}

char *
netclip_net_recv(int fd)
{
  /*

    FIXME: Something is wrong with the sending and recieving of data

  */

  int status;

  // first, get size of data
  int size = __read_int(fd);
  if (size == -2 || size <= 0) /* Specifc error code: no -2's should be sent */
    {
#if DEBUG
      syslog(LOG_ERR, "Error reading command");
#endif
      return NULL;
    }

#if DEBUG
  syslog(LOG_INFO, "Size of buffer read: %d\n", size);
#endif

  char *buffer = malloc(size);
  if (!buffer)
    {
#ifdef IS_DAEMON
      syslog(LOG_ERR, "Error allocating buffer");
#else
      fprintf(stderr, "Error allocating buffer\n");
#endif
      return NULL;
    }

  status = read(fd, buffer, size);
  if (status <= 0)
    {
#ifdef IS_DAEMON
      syslog(LOG_ERR, "Error in read function");
      syslog(LOG_ERR, "Status: %d", status);
#else
      fprintf(stderr, "Error in read function\n");
#endif
      return NULL;
    }

  // FIXME: could this be a problem?
  buffer[size] = '\0';

#if DEBUG
#ifdef IS_DAEMON
  syslog(LOG_INFO, "Buffer recieved: %s\n", buffer);
#else
  printf(ANSI_COLOR_YELLOW "Buffer recieved: %s\n" ANSI_COLOR_RESET, buffer);
#endif
#endif

  return buffer;
}

int
netclip_net_send_cmd(int fd, int cmd)
{
  return __write_int(fd, cmd);
}

int
netclip_net_recv_cmd(int fd)
{
  return __read_int(fd);
}

void
netclip_net_close(int fd)
{
  close(fd);
}

static int
__write_int(int fd, int32_t val)
{
  int status;
  int32_t write_val = htonl(val);

  status = write(fd, &write_val, sizeof(int32_t));
  if (status <= 0)
    return -1;

  return status;
}

static int
__read_int(int fd)
{
  int     status;
  int32_t val;

  status = read(fd, &val, sizeof(int32_t));
  if (status <= 0)
    return -2;

  val = ntohl(val);

  return val;
}
