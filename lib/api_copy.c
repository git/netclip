
/*

  clip_stack.c -- defininition of clip stack routines
  Copyright (C) 2017  Charlie Sale

  This file is part of netclip

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include <netclip.h>
#include <stdio.h>
#include <stdlib.h>

int
nc_copy(nc_session_t *session, char *buffer)
{
  int ret;

  // send command
  ret = netclip_net_send_cmd(session->conn, NCR_CREAT);
  if (ret < 0)
    return -1;

  // send data
  ret = netclip_net_send(session->conn, clip_new(buffer, NULL));
  if (ret < 0)
    return -1;

  return 0;
}
