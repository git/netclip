
/*

  clip_stack.c -- defininition of clip stack routines
  Copyright (C) 2017  Charlie Sale

  This file is part of netclip

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include <netclip.h>
#include <stdio.h>
#include <stdlib.h>
#include <ifaddrs.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netdb.h>
#include <unistd.h>
#include <linux/if_link.h>

int
nc_init(nc_session_t *session, char *host, int port)
{
  int ret;

  if (host == NULL)
    host = "127.0.0.1";

  /* Connect to server */
  session->conn = netclip_cli_init(host, port);
  if (session->conn < 0)
    return -1;

  /* stat stack */
  // TODO: Need to write server side for this function

  return 0;
}

int
nc_close(nc_session_t *session)
{
  netclip_net_close(session->conn);

  // these may be stack allocated, so they might need to get changed
  // FIXME: This is causing some errors
  // if (session->clip) free(session->clip);

  // this should be heap allocated
  //  free(session);

  return 0;
}
