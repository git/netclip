

/*

  netclipd_main.c -- main entry point for netclipd
  Copyright (C) 2017  Charlie Sale

  This file is part of netclip

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include <netclip.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <sys/stat.h>
#include <syslog.h>
#include "funcs.h"
#include <config.h>

// global variables
clip_stack_t *root_stack; // this is the stack for the clippings

/* TODO: add some commandline options */

netclip_sock_t server; // the server socket description

int main(int argc, char **argv)
{
  int ret; // general purpose return value


  /* Start logging */
  start_log();

  /*
    If true, then all of this code is not needed because the init system does
    that for us.
   */
#if WITH_SYSV || WITH_OPENRC

  /* Daemon Setup */
  pid_t pid, sid;

  pid = fork ();
  if (pid < 0)
    {
      log_code(NO_FORK);
      closelog();
      exit(EXIT_FAILURE);
    }

  if (pid > 0)
    {
#if DEBUG
      printf("Child process id: %d\n", pid);
#endif
      exit (EXIT_SUCCESS);
    }

  // change file mask
  umask (0);

  // create new session
  sid = setsid ();
  if (sid < 0)
    {
      log_code(NO_SETSID);
      closelog();
      exit(EXIT_FAILURE);
    }

  // change directory
  if ((chdir ("/")) < 0)
    {
      log_code(NO_CHDIR);
      closelog();
      exit(EXIT_FAILURE);
    }

  // close standard I/O
  close (STDIN_FILENO);
  close (STDOUT_FILENO);
  close (STDERR_FILENO);

#endif // SYSV

  /* Setup stack and start server */
  // NOTE: this may be auto handled by an init system
  root_stack = clip_stack_new(32); // TODO: make this an option
  if (!root_stack)
    {
      log_code(NO_STACK_MAKE);
      closelog();
      exit(EXIT_FAILURE);
    }

  ret = start_server(server);
  if (ret != 0)
    {
      log_code(NO_CONNECT);
      free (root_stack);
      free (root_stack->clip_array);
      closelog();
      exit(EXIT_FAILURE);
    }

  // set server to listen with 3 backlog
  listen(server, 5);

  syslog(LOG_INFO, "Server started");

  // this is the actual daemon loop. Return whatever it's return code is
  ret = main_loop(server, root_stack);

  /* Error encountered*/
  if (ret != 0)
    {
      log_code(OTHER);
      close_daemon(server, root_stack, EXIT_FAILURE);
    }

  syslog(LOG_INFO, "Successful execution");

  close_daemon(server, root_stack, EXIT_SUCCESS);

  return ret;
}
