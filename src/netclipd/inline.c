
/*

  inline.c -- Implementation of smaller functions
  Copyright (C) 2017  Charlie Sale

  This file is part of netclip

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "funcs.h"
#include <netclip.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <syslog.h>
#include <config.h>

static char *err_msgs[] =
  {
    "Error: Could not start server",
    "Error: Could not read data",
    "Error: Could not  write data",
    "Error: Memory exhausted (could not allocate pointer)",
    "Error: Clipstack full",
    "Error: Could not create new clip",
    "Error: Could not push clip to stack",
    "Error: Could not pop clip from stack",
    "Error: Could not get clip from stack",
    "Error: Could not fork session",
    "Error: Could not change SID",
    "Error: Could not change directory",
    "Error: Could not allocate stack",
    "Error: Could not accept client",
    "Error: Could not dump string into buffer",
    "Error: Command not found",
    "Error: Undefined error encountered"
  };


inline void
start_log()
{
  // Only open the syslog connection
  openlog("netclipd", LOG_CONS | LOG_PID, LOG_DAEMON);
}

inline void
log_code(enum err c)
{
  // log message based on what code is
  syslog(LOG_ERR, "%s", err_msgs[c]);
}

void
close_daemon(int sock, clip_stack_t *stack, int status)
{
  // close the socket, free the clipstack, and exit
  close(sock);
  clip_stack_free(stack);
  syslog(LOG_INFO, "netclipd is closing");
  closelog();
  exit(status);
}
