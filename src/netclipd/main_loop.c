
/*

  main_loop.c -- the actual main loop of the daemon

  Copyright (C) 2017  Charlie Sale

  This file is part of netclip

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "funcs.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netclip.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <memory.h>
#include <config.h>

static int
__handle_conn(int, clip_stack_t*, char*, int*);

static int
__accept_conn(int server, char *host_buf);

static int
__handle_create(int conn, clip_stack_t *stack, char *host);

static int
__handle_read(int conn, clip_stack_t *stack);

static int
__handle_get(int conn, clip_stack_t *stack);

static void
__dump_stack(clip_stack_t *stack);

int
main_loop(int server, clip_stack_t *stack)
{
  int ret; // gen purpose return value

  while (1)
    {
      // read a connection

      char host_buf[16]; // for storing the client's host

      int conn = __accept_conn(server, host_buf);
      if (conn < 0)
        {
          log_code(NO_ACCEPT);
          return 1;
        }
      else
        {
          syslog(LOG_INFO, "Connection accepted\n");
        }

      // if daemon should be killed
      int kill = 0;

      int ret = __handle_conn(conn, stack, host_buf, &kill);
      if (ret < 0)
        {
          fprintf(stderr, "Error handling connection\n");
          return -1;
        }

      // after each connection, dump the stack for debugging
#if DEBUG
      __dump_stack(stack);
#endif

      if (kill) break;
    }

  return 0; // if loop is broken and not returned, it is successful
}

static int
__accept_conn(int server, char* host_buf)
{
  int fd;

  struct sockaddr_storage conn_addr;
  socklen_t addrsize = sizeof(conn_addr);

  // accept conn
  fd = accept(server, (struct sockaddr *) &conn_addr, &addrsize);
  if (fd < 0)
    return -1;

  struct sockaddr_in * addr = (struct sockaddr_in *) &conn_addr;

  // this may throw an error
  if (host_buf)
    {
      #warning Host_buffer will be set to NULL
      host_buf = NULL;
    }

  return fd;
}

static int
__handle_conn(int conn, clip_stack_t *stack, char *host_buf, int *kill)
{
  int ret;
  switch(netclip_net_recv_cmd(conn))
    {
      /* TODO: Change the numbers to macros */

      case NCR_CREAT: /* Create */
#if DEBUG
          syslog(LOG_INFO, "Recieved command create");
#endif
          ret = __handle_create(conn, stack, host_buf);
          if (ret) return 1;

          break;

        case NCR_READ: /* Read */
#if DEBUG
          syslog(LOG_INFO, "Recieved command read");
#endif

          ret = __handle_read(conn, stack);
          if (ret < 0) return 1;

          break;

        case NCR_GET: /* get */
#if DEBUG
          syslog(LOG_INFO, "Recieved command get");
#endif

          ret = __handle_get(conn, stack);
          if (ret < 0) return 1;

          break;

        case NCR_KILL: /* Nothing */
#if DEBUG
          syslog(LOG_INFO, "Recieved kill command");
#endif
          *kill = 1; // let nothing kill the daemon

          break;

        default: /* Error */
          log_code(NO_CMD);
          return 1;
          break;

        }
      // close the connection
      close (conn);
      return 0;
}

static int
__handle_create(int conn, clip_stack_t *stack, char *host)
{
  // read clipping data
  int ret;
  char *buffer = netclip_net_recv(conn);
  if (!buffer)
    {
      log_code(READ_ERR);
      return -1;
    }

  // store data into node
  clip_t *clip = clip_new(buffer, host);
  if (!clip)
    {
      log_code(MEM_EXHAUST);
      return -1;
    }

  // push node to stack
  ret = clip_stack_push(stack, clip);
  if (ret < 0)
    {
      log_code(PUSH_ERR);
      return -1;
    }

  free(buffer);

  return 0; // success
}

static int
__handle_read(int conn, clip_stack_t *stack)
{
  // get (not pop) top node
  clip_t *clip = clip_stack_get_top(stack);
  if (!clip)
    {
      log_code(GET_ERR);
      return -1;
    }

  // send data to connection
  int ret = netclip_net_send(conn, clip);
  if (ret < 0)
    {
      log_code(WRITE_ERR);
      return -1;
    }

  return 0;
}

static int
__handle_get(int conn, clip_stack_t *stack)
{
  // pop top node
  clip_t *clip = clip_stack_pop(stack);
  if (!clip)
    {
      log_code(POP_ERR);
      return -1;
    }

  // send data to connection
  int ret = netclip_net_send(conn, clip);
  if (ret < 0)
    {
      log_code(WRITE_ERR);
      return -1;
    }

  // done
  return 0;
}

static int
__handle_nothing(void)
{
  return 0; // There is nothing to really do for this for now...
}

/* Just a debugging function to output the contents of the stack */
static void
__dump_stack(clip_stack_t *stack)
{
  #warning "__dump_stack is not implemented"
}
