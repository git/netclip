
/*

  funcs.h -- This file contains definitions of functions to be used
                      in netclipd
  Copyright (C) 2017  Charlie Sale

  This file is part of netclip

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef NETCLIPD_FUNC_H
#define NETCLIPD_FUNC_H

#include <netclip.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <syslog.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <signal.h>
#include <config.h>

enum err
  {
    NO_CONNECT    = 0,
    READ_ERR      = 1,
    WRITE_ERR     = 2,
    MEM_EXHAUST   = 3,
    STACK_FULL    = 4,
    NO_CLIP_MAKE  = 5,
    PUSH_ERR      = 6,
    POP_ERR       = 7,
    GET_ERR       = 8,
    NO_FORK       = 9,
    NO_SETSID     = 10,
    NO_CHDIR      = 11,
    NO_STACK_MAKE = 12,
    NO_ACCEPT     = 13,
    DUMP_ERR      = 14,
    NO_CMD        = 15,
    OTHER         = 16
  };

/* Initiates logging */
void
start_log();

void
log_code(enum err c);

/* Frees daemon resources */
void
close_daemon(int, clip_stack_t*, int);

/* Create server and exit if failed */
int
start_server(int);

/* Actual main loop for daemon */
int
main_loop(int, clip_stack_t*);

/* Signal handling */
void
on_sigterm(int);

#endif // NETCLIPD_FUNC_H
