/*

  command_handlers.c -- Handlers for each command
  Copyright (C) 2017  Charlie Sale

  This file is part of netclip

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include <netclip.h>
#include <stdio.h>
#include <stdlib.h>
#include "funcs.h"
#include <config.h>


static int
__get_fsize(FILE*);

static char*
__read_input(netclip_opts_t);

static void
__set_file(netclip_opts_t);

int
handle_copy(int conn, netclip_opts_t options)
{
  int ret; // general purpose return value

  __set_file(options); // quick function to set file if not already set

  // first, read input
  char *buffer = __read_input(options);

  // make a new clipping
  clip_t *clip = clip_new(buffer, ((options.host) ? options.host : NULL ));
  if (!clip)
    {
      fprintf(stderr, "Error creating new clipping\n");
      return -1;
    }

  ret = netclip_net_send_cmd(conn, NCR_CREAT);
  if (ret < 0)
    {
      fprintf(stderr, ANSI_COLOR_RED "Error sending command\n" ANSI_COLOR_RESET);
      return -1;
    }

  ret = netclip_net_send(conn, clip);
  if (ret < 0)
    {
      fprintf(stderr, ANSI_COLOR_RED "Error sending clipping\n" ANSI_COLOR_RESET);
      return -1;
    }

  free(buffer);

  return 0;
}

int
handle_paste(int conn, netclip_opts_t options)
{
  int ret;

  // send command
  ret = netclip_net_send_cmd(conn, NCR_READ);
  if (ret < 0)
    {
      fprintf(stderr, "Error sending command\n");
      return -1;
    }

  // read clipping
  char *buf = netclip_net_recv(conn);
  if (!ret)
    {
      fprintf(stderr, "Could not read data\n");
      return -1;
    }

  fprintf(options.fd, "%s\n", buf);

  free(buf);

  // done

  return 0;
}

int
handle_get(int conn, netclip_opts_t options)
{
  int ret;

  // send command
  ret = netclip_net_send_cmd(conn, NCR_GET);
  if (ret < 0)
    {
      fprintf(stderr, "Could not send command\n");
      return -1;
    }

  // read data
  char *buf = netclip_net_recv(conn);
  if (!buf)
    {
      fprintf(stderr, "Error recieving data\n");
      return -1;
    }

  fprintf(options.fd, "%s\n", buf);

  free(buf);

  return 0;
}


static int
__get_fsize(FILE *f)
{
  fseek(f, 0L, SEEK_END);
  int s = ftell(f);
  rewind(f);

  return s;
}


static char*
__read_input(netclip_opts_t options)
{
  char *buf = calloc(0, sizeof(char)); // make a zero length pointer
  if (!buf)
    {
      fprintf(stderr, "Error allocating memeory\n");
      return NULL;
    }

  int size = 0;
  int ch;
  while ((ch = fgetc(options.fd)) != EOF)
    {
      buf = realloc(buf, sizeof(buf) + 1); // append a char each time
      if (!buf)
        {
          fprintf(stderr, "Error allocating memory\n");
          return NULL;
        }

      buf[size++] = ch; // append the char
    }

  buf = realloc(buf, sizeof(buf) + 1);
  if (!buf)
    {
      fprintf(stderr, "Error allocating memory\n");
      return NULL;
    }

  buf[size] = '\0'; // add null terminator

  return buf;
}

static void
__set_file(netclip_opts_t options)
{
  if (!options.fd)
    {
      if (options.copy)
        {
          options.fd = stdout;
        }
      else if (options.paste || options.get)
        {
          options.fd = stdin;
        }
      else
        {
          options.fd = NULL;
        }
    }

}
