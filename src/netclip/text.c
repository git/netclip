

/*

  text.c -- Handles printing functions (print_help, print_version)
  Copyright (C) 2017  Charlie Sale

  This file is part of netclip

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/


#include <stdio.h>
#include "funcs.h"
#include <config.h>

void
print_help()
{
  printf
    ("%s -- interface for netclip clipboard."
     "\n"
     "netclip is used for interfacing with the netclip package. This program\n"
     "is only the shell front end. This provides usage within the shell.\n"
     "Usage:\n"
     "netclip [option(s)] [arguments]\n"
     "Options:\n\n"
     "-h --host    : select the host to connect to\n"
     "-c --copy    : copy [text] to clipboard\n"
     "-p --paste   : retrieves the top item from the stack and prints it\n"
     "-g --get     : like paste, but it pops the entry\n"
     "   --help    : print this screen\n"
     "-v --version : show version info\n"
     "--verbose    : show verbose printing\n"
     "\n"
     "Report bugs to netclip-bug@nongnu.org\n"
     "Package homepage <https://savannah.nongnu.org/projects/netclip/>\n"
     "General help using GNU software: <http://www.gnu.org/gethelp>\n"
     , PACKAGE_STRING);
}

// TODO: add gnu stuff
void
print_version()
{
  printf
    ("%s\n", PACKAGE_STRING);
}
