

/*

  funcs.h -- Definition of functions only used by netclip
  Copyright (C) 2017  Charlie Sale

  This file is part of netclip

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include <netclip.h>
#include <getopt.h>
#include <stdio.h>
#include <config.h>

/* All program options */
typedef struct
{
  int   version;   // if print version text
  int   help;      // if print help screen
  int   verbose;   // if verbose printing
  char *host;      // if !null, host to send (default, localhost)
  int   copy;      // if !null, text to copy
  int   paste;     // if paste text
  int   get;       // if get text
  FILE *fd;        // file descriptor to output to to input from
} netclip_opts_t;


/* Note, all aplicable functions must pass netclip_opts_t structure */

/* Shows help text */
void
print_help();

/* Prints version text */
void
print_version();

/* Get's options */
netclip_opts_t
get_options(int, char**);

int
handle_copy(int, netclip_opts_t);

int
handle_paste(int, netclip_opts_t);

int
handle_get(int, netclip_opts_t);
