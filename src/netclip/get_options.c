
/*

  get_options.c -- function that contains getopt_long
  Copyright (C) 2017  Charlie Sale

  This file is part of netclip

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include <netclip.h>
#include <getopt.h>
#include "funcs.h"
#include <unistd.h>

netclip_opts_t options;

static int opt_long_index;

static struct option arg_options[] =
  {
    { "copy",    no_argument,       NULL, 'c' },
    { "paste",   no_argument,       NULL, 'p' },
    { "get",     no_argument,       NULL, 'g' },
    { "file",    required_argument, NULL, 'f' },
    { "help",    no_argument,       NULL, 'h' },
    { "verbose", no_argument,       NULL, 'V' },
    { "version", no_argument,       NULL, 'v' },
    { "host",    optional_argument, NULL, 'i' },
    {0,0,0,0}
  };

static const char *opt_str = "cpghVvf:i:";

static void
__init_options(void);

netclip_opts_t
get_options(int argc, char **argv)
{
  __init_options(); // set defaults of argument structure

  int c;

  while ((c = getopt_long(argc, argv, opt_str, arg_options, NULL)) != -1)
    {
      switch (c)
        {
        case 'v':
          options.version = 1;
          break;

        case 'V':
          options.verbose = 1;
          break;

        case 'c':
          options.copy = 1;
          break;

        case 'p':
          options.copy = 1;
          break;

        case 'h':
          options.help = 1;
          break;

        case 'g':
          options.get = 1;
          break;

        case 'f':
          if (optarg)
            options.fd = fopen(optarg, "r+");

          if (!options.fd)
            options.fd = NULL;
          break;

        case 'i':
          if (optarg)
            options.host = optarg;
          else
            options.host = "127.0.0.1";

        case ':':
	case '?':
	default:
	    abort();
        }
    }

  return options;
}

static void
__init_options(void)
{
  options.copy    = 0;
  options.get     = 0;
  options.version = 0;
  options.help    = 0;
  options.verbose = 0;
  options.host    = NULL;
  options.paste   = 0;
  options.fd      = NULL;
}
