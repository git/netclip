

/*

  main.c -- main entry point for netclip
  Copyright (C) 2017  Charlie Sale

  This file is part of netclip

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include <netclip.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <getopt.h>
#include "funcs.h"
#include <config.h>

#define IS_DAEMON 0

/* constants for commands */
enum
  {
    CMD_PASTE = 3,
    CMD_COPY = 1,
    CMD_GET = 2,
    CMD_HELP = 4,
    CMD_VERSION = 5
  };

/* Define a few constants */

netclip_sock_t client; // client socket descriptor

static netclip_opts_t options;

/* Static functions */
static int
__get_cmd(netclip_opts_t);

static int
__init_cli(netclip_sock_t, netclip_opts_t);

int main(int argc, char **argv)
{
  int ret;

  /* Get options */
  options = get_options(argc, argv);

  /* There is no error checking here because
     the function outputs an error message.
     Plus, individual program parts can simply
     test before executing
   */

  /* Initiate connection */
#if OFFLINE_ENABLED
  #warning "Skipped server connection code. Features may not function"
#else
  ret = __init_cli(client, options);
  if (ret < 0)
    {
      fprintf(stderr, "Error connecting to server\n");
      return 1;
    }
#endif

  /* Handle command */
  switch(__get_cmd(options))
    {
    case CMD_COPY:
      ret = handle_copy(client, options);
      if (ret < 0)
        return 1;
      break;

    case CMD_GET:
      ret = handle_paste(client, options);
      if (ret < 0)
        return 1;
      break;

    case CMD_PASTE:
      ret = handle_get(client, options);
      if (ret < 0)
        return 1;
      break;

    case CMD_HELP:
      print_help();
      break;

    case CMD_VERSION:
      print_version();
      break;

    default:
      fprintf(stderr, "Error handling command\n");
      return 1;
      break;
    }

  return 0;
}


static int
__get_cmd(netclip_opts_t opts)
{
  if (opts.copy)
    return 1;
  else if (opts.get)
    return 2;
  else if (opts.paste)
    return 3;
  else if (opts.help)
    return 4;
  else if (opts.version)
    return 5;
  else
    return -1;
}


static int
__init_cli(netclip_sock_t cli, netclip_opts_t options)
{
  cli = netclip_cli_init(options.host, 8899);
  if (cli < 0)
    return -1;

  return 0;
}
